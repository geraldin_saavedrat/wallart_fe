import { createRouter, createWebHashHistory } from "vue-router";
import App from "../App.vue";
import LogIn from "../components/LogIn.vue";
import Home from "../components/Home.vue";
import SingUp from "../components/SingUp.vue";
const routes = [
  {
    path: "/",
    name: "root",
    component: App,
  },
  {
    path: "/login",
    name: "LogIn",
    component: LogIn,
  },
  {
    path: "/Home",
    name: "Home",
    component: Home,
  },
  {
    path: "/SingUp",
    name: "SingUp",
    component: SingUp,
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
